// https://blog.golang.org/go-slices-usage-and-internals
// https://gist.github.com/panamafrancis/b97d0a52d9fcbd300836842dc2a1706a
// https://medium.com/@blumenmann/a-simple-beginners-tutorial-to-io-writer-in-golang-2a13bfefea02#.cr8w9me61

// build problem: env GOOS=linux GOARCH=amd64 go build
// http://stackoverflow.com/questions/36198418/golang-cannot-execute-binary-file-exec-format-error
//
// fun go in background:
// http://stackoverflow.com/questions/12486691/how-do-i-get-my-golang-web-server-to-run-in-the-background
//
// connect contaier db error: https://github.com/docker-library/mysql/issues/81
// https://docs.docker.com/compose/startup-order/
package main

import (
	"alarm-server/chore/database"
	"alarm-server/controllers"
	"alarm-server/models"
)

func main() {
	database.Create("alarm")

	models.OpenDB()
	controllers.Service(":9090")
}
