package models

import (
	"alarm-server/utilts"
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

var Err error
var DB *sql.DB

func OpenDB() {
	DB, Err = sql.Open("mysql", "root:1234@tcp(mariadb:3306)/alarm")
	utilts.CheckErr(Err)
}
