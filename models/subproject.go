package models

import (
	"alarm-server/utilts"
	"database/sql"
)

func CreateSub(projectid string, label string, content string, t string) sql.Result {
	query := `
    INSERT subproject SET
    project_id=?,
    label=?,
    content=?,
    create_time=?,
    alarm_cycle_type=?,
    is_alarm=?
  `

	//插入数据
	stmt, err := DB.Prepare(query)
	utilts.CheckErr(err)

	res, err := stmt.Exec(projectid, label, content, t, "", true)
	utilts.CheckErr(err)

	return res
}

func DeleteSub(id string, subid string) {
	query := `
    DELETE FROM subproject
    WHERE project_id=? AND sub_id=?
  `

	//插入数据
	stmt, err := DB.Prepare(query)
	utilts.CheckErr(err)

	stmt.Exec(id, subid)
}

func ModifySub(label string, subid string, id string) {
	query := `
    UPDATE subproject
    SET label=?
    WHERE sub_id=? AND project_id=?
  `

	//插入数据
	stmt, err := DB.Prepare(query)
	utilts.CheckErr(err)

	stmt.Exec(label, subid, id)
}

func ResolveSub(subid string, id string) {
	query := `
		UPDATE subproject
		SET is_alarm=?
		WHERE sub_id=? AND project_id=?
	`

	//插入数据
	stmt, err := DB.Prepare(query)
	utilts.CheckErr(err)
	stmt.Exec(false, subid, id)
}

func SelectAllSub() *sql.Rows {
	rows, err := DB.Query("SELECT * FROM Subproject")
	utilts.CheckErr(err)

	return rows
}
