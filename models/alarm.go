package models

import "alarm-server/utilts"

// http://stackoverflow.com/questions/21108084/golang-mysql-insert-multiple-data-at-once?answertab=votes#tab-top
func CreateAlarm(alarmCycle []string, subid int64, projectid string) {
	alarmQuery := "INSERT INTO alarmCycle(date, sub_id, project_id) VALUES "
	vals := []interface{}{}

	for _, v := range alarmCycle {
		alarmQuery += "(?, ?, ?),"
		vals = append(vals, v, subid, projectid)
	}

	//trim the last ,
	alarmQuery = alarmQuery[0 : len(alarmQuery)-1]

	//prepare the statement
	alarmStmt, err := DB.Prepare(alarmQuery)
	utilts.CheckErr(err)

	//format all vals at once
	alarmStmt.Exec(vals...)
}
