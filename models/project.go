package models

import (
	"alarm-server/utilts"
	"database/sql"
)

func CreateProject(label string, content string, t string) sql.Result {
	stmt, err := DB.Prepare("INSERT project SET label=?,content=?,create_time=?")
	utilts.CheckErr(err)

	res, err := stmt.Exec(label, content, t)
	utilts.CheckErr(err)

	return res
}

func DeleteProject(id string) {
	query := `
		DELETE FROM project
		WHERE project_id=?
	`

	stmt, err := DB.Prepare(query)
	utilts.CheckErr(err)

	stmt.Exec(id)
}

func ModifyProject(id string, label string) {
	query := `
		UPDATE project
		SET label=?
		WHERE project_id=?
	`

	//插入数据
	stmt, err := DB.Prepare(query)
	utilts.CheckErr(err)

	stmt.Exec(label, id)
}

func SelectAllProject() *sql.Rows {
	rows, err := DB.Query("SELECT * FROM project")
	utilts.CheckErr(err)

	return rows
}
