// Why Golang cannot generate json from struct with front lowercase character?
// http://stackoverflow.com/questions/21825322/why-golang-cannot-generate-json-from-struct-with-front-lowercase-character

package structs

type Window struct {
	Height int
	Width  int
}

type sub []Sub

type Project struct {
	ID         int    `json:"id"`
	Label      string `json:"label"`
	CreateTime string `json:"create_time"`
	Content    string `json:"content"`
	Subs       sub    `json:"subs"`
}

type Sub struct {
	SubID          int    `json:"sub_id"`
	CreateTime     string `json:"create_time"`
	Label          string `json:"label"`
	Content        string `json:"content"`
	AlarmCycleType string `json:"alarm_cycle_type"`
	// IssAlarm 每日統一寫入一次
	IsAlarm   bool `json:"is_alarm"`
	ProjectID int  `json:"project_id"`
	// ReviewInto []int  `json:"review_info"`
}

type AlarmCycleType struct {
	DateID    int    `json:"date_id"`
	Date      string `json:"date"`
	SubID     int    `json:"sub_id"`
	ProjectID int    `json:"project_id"`
}

type Record struct {
	RecordID   int    `json:"record_id"`
	CreateTime string `json:"create_time"`
	SubID      int    `json:"sub_id"`
}
