package main

import (
	"alarm-server/structs"
	"alarm-server/utilts"
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func stopAlarm() {
	db, err := sql.Open("mysql", "root:1234@tcp(mariadb:3306)/")
	checkErr(err)
	defer db.Close()

	_, err = db.Exec("USE alarm")
	checkErr(err)

	_, err = db.Query("UPDATE subproject SET is_alarm=false")
	checkErr(err)
}

func getAlarm() []structs.AlarmCycleType {
	db, err := sql.Open("mysql", "root:1234@tcp(mariadb:3306)/")
	checkErr(err)
	defer db.Close()

	_, err = db.Exec("USE alarm")
	checkErr(err)

	date := utilts.NowFormat()

	rows, err := db.Query("SELECT * FROM alarmCycle WHERE date=?", date)
	checkErr(err)

	AlarmCycleTypes := []structs.AlarmCycleType{}

	for rows.Next() {
		var AlarmCycleType structs.AlarmCycleType

		err = rows.Scan(
			&AlarmCycleType.DateID,
			&AlarmCycleType.Date,
			&AlarmCycleType.SubID,
			&AlarmCycleType.ProjectID,
		)

		checkErr(err)
		AlarmCycleTypes = append(AlarmCycleTypes, AlarmCycleType)
	}

	return AlarmCycleTypes
}

func update(rows []structs.AlarmCycleType) {
	db, err := sql.Open("mysql", "root:1234@tcp(mariadb:3306)/")
	checkErr(err)
	defer db.Close()

	_, err = db.Exec("USE alarm")
	checkErr(err)

	alarmQuery := "UPDATE subproject SET is_alarm=true WHERE "

	vals := []interface{}{}

	for _, row := range rows {
		alarmQuery += "sub_id=? and project_id=? or "

		vals = append(vals, row.SubID, row.ProjectID)
	}

	//trim the last ,
	alarmQuery = alarmQuery[0 : len(alarmQuery)-3]
	//prepare the statement
	alarmStmt, err := db.Prepare(alarmQuery)
	checkErr(err)

	//format all vals at once
	alarmStmt.Exec(vals...)
}

func main() {
	stopAlarm()
	rows := getAlarm()
	update(rows)
}
