package database

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func Create(name string) {
	createProjectTable := `
		CREATE TABLE IF NOT EXISTS project(
			project_id INT NOT NULL AUTO_INCREMENT,
			label VARCHAR(32),
			create_time VARCHAR(15),
			content VARCHAR(150),
			PRIMARY KEY (project_id)
		)
		DEFAULT CHARACTER SET = utf8
		COLLATE = utf8_general_ci
	`
	createSubprojectTable := `
		CREATE TABLE IF NOT EXISTS subproject(
			sub_id INT NOT NULL AUTO_INCREMENT,
			create_time VARCHAR(15),
			label VARCHAR(32),
			content VARCHAR(150),
			alarm_cycle_type VARCHAR(30),
			is_alarm BOOLEAN,
			project_id INT NOT NULL,
			PRIMARY KEY (sub_id)
		)
		DEFAULT CHARACTER SET = utf8
		COLLATE = utf8_general_ci
	`

	createAlarmCycleTable := `
		CREATE TABLE IF NOT EXISTS alarmCycle(
			date_id INT NOT NULL AUTO_INCREMENT,
			date VARCHAR(15) NOT NULL,
			sub_id INT NOT NULL,
			project_id INT NOT NULL,
			PRIMARY KEY (date_id)
		)
		DEFAULT CHARACTER SET = utf8
		COLLATE = utf8_general_ci
	`

	createRecordTable := `
		CREATE TABLE IF NOT EXISTS record(
			record_id INT NOT NULL AUTO_INCREMENT,
			create_time int,
			sub_id INT NOT NULL,
			PRIMARY KEY (record_id)
		)
	`

	db, err := sql.Open("mysql", "root:1234@tcp(mariadb:3306)/")
	checkErr(err)
	defer db.Close()

	_, err = db.Exec("CREATE DATABASE IF NOT EXISTS " + name)
	checkErr(err)

	fmt.Println("Create Database: " + name + " Success")

	_, err = db.Exec("USE " + name)
	checkErr(err)

	_, err = db.Exec(createProjectTable)
	checkErr(err)

	fmt.Println("Create Project Table Success")

	_, err = db.Exec(createSubprojectTable)
	checkErr(err)

	fmt.Println("Create Subproject Table Success")

	_, err = db.Exec(createAlarmCycleTable)
	checkErr(err)

	fmt.Println("Create Alarm Cycle Table Success")

	_, err = db.Exec(createRecordTable)
	checkErr(err)

	fmt.Println("Create Record Table Success")
}
