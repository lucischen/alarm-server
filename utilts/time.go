package utilts

import "time"

func Now() int64 {
	var t int64
	t = time.Now().UTC().Unix()
	return t
}

func NowFormat() string {
	return time.Now().UTC().Format("2006-01-02")
}

func Millisecond(now int64, day int64) int64 {
	return now + (day * 24 * 60 * 60)
}
