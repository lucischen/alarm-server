package utilts

import (
	"encoding/json"
	"net/http"
)

func Unmarshal(r *http.Request) map[string]string {
	data := r.Form["sub"][0]
	m := map[string]string{}
	json.Unmarshal([]byte(data), &m)

	return m
}

func SetHeader(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	r.ParseMultipartForm(0)

	w.Header().Set("Content-Type", "application/json")
}
