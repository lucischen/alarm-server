package subproject

import (
	"alarm-server/controllers/collection"
	"alarm-server/models"
	"alarm-server/utilts"
	"encoding/json"
	"net/http"
)

func Delete(w http.ResponseWriter, r *http.Request) {
	utilts.SetHeader(w, r)

	m := utilts.Unmarshal(r)

	id := m["id"]
	subid := m["subid"]

	models.DeleteSub(id, subid)

	projects := collection.SelectProject()

	js, err := json.Marshal(projects)
	utilts.CheckErr(err)

	w.Write(js)
}
