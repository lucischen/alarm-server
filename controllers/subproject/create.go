package subproject

import (
	"alarm-server/business"
	"alarm-server/controllers/collection"
	"alarm-server/models"
	"alarm-server/utilts"
	"encoding/json"
	"fmt"
	"net/http"
)

func Create(w http.ResponseWriter, r *http.Request) {
	utilts.SetHeader(w, r)

	m := utilts.Unmarshal(r)

	projectid := m["id"]
	label := m["label"]
	content := m["content"]

	alarmCycle := business.CreateCycle()

	t := utilts.NowFormat()

	res := models.CreateSub(projectid, label, content, t)

	subid, err := res.LastInsertId()
	utilts.CheckErr(err)

	models.CreateAlarm(alarmCycle, subid, projectid)

	fmt.Println("insert success")

	projects := collection.SelectProject()

	js, err := json.Marshal(projects)
	utilts.CheckErr(err)

	w.Write(js)
}
