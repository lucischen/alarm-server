package controllers

import (
	"alarm-server/controllers/collection"
	"alarm-server/utilts"
	"encoding/json"
	"net/http"
)

// http://stackoverflow.com/questions/17265463/how-do-i-convert-a-database-row-into-a-struct-in-go
// http://www.alexedwards.net/blog/golang-response-snippets
func SelectAll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	projects := collection.SelectProject()

	js, err := json.Marshal(projects)
	utilts.CheckErr(err)

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
