package collection

import (
	"alarm-server/models"
	"alarm-server/structs"
)

func SelectProject() []structs.Project {
	rows := models.SelectAllProject()
	sub := SelectSubproject()

	numbers := make(map[int][]structs.Sub)

	for _, s := range sub {

		numbers[s.ProjectID] = append(numbers[s.ProjectID], s)

	}

	projects := []structs.Project{}

	for rows.Next() {
		var project structs.Project

		rows.Scan(
			&project.ID,
			&project.Label,
			&project.CreateTime,
			&project.Content,
		)

		project.Subs = numbers[project.ID]

		projects = append(projects, project)
	}

	return projects
}
