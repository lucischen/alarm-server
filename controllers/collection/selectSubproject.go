package collection

import (
	"alarm-server/models"
	"alarm-server/structs"
)

func SelectSubproject() []structs.Sub {
	rows := models.SelectAllSub()

	Subs := []structs.Sub{}

	for rows.Next() {
		var Sub structs.Sub

		rows.Scan(
			&Sub.SubID,
			&Sub.CreateTime,
			&Sub.Label,
			&Sub.Content,
			&Sub.AlarmCycleType,
			&Sub.IsAlarm,
			&Sub.ProjectID,
		)

		Subs = append(Subs, Sub)
	}

	return Subs
}
