package controllers

import (
	"alarm-server/controllers/project"
	"alarm-server/controllers/subproject"
	"log"
	"net/http"
)

func Service(port string) {
	http.HandleFunc("/requestCreate", project.Create)
	http.HandleFunc("/modifyProjectName", project.Modify)
	http.HandleFunc("/deleteProject", project.Delete)
	http.HandleFunc("/createSub", subproject.Create)
	http.HandleFunc("/modifySubprojectName", subproject.Modify)
	http.HandleFunc("/deleteSubproject", subproject.Delete)
	http.HandleFunc("/resolveMission", subproject.Resolve)
	http.HandleFunc("/gg", SelectAll)

	err := http.ListenAndServe(port, nil) //设置监听的端口
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
