package project

import (
	"alarm-server/controllers/collection"
	"alarm-server/models"
	"alarm-server/utilts"
	"encoding/json"
	"fmt"
	"net/http"
)

func Create(w http.ResponseWriter, r *http.Request) {
	utilts.SetHeader(w, r)

	m := utilts.Unmarshal(r)

	label := m["label"]
	content := m["content"]

	t := utilts.NowFormat()
	//插入数据

	models.CreateProject(label, content, t)

	fmt.Println("insert success")

	projects := collection.SelectProject()

	js, err := json.Marshal(projects)
	utilts.CheckErr(err)

	w.Write(js)
}
