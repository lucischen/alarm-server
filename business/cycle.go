package business

import (
	"alarm-server/utilts"
	"time"
)

func CreateCycle() []string {
	TIMEFORMAT := "2006-01-02"
	var sl = []string{}

	var loop = [11]int64{0, 1, 3, 7, 15, 24, 31, 40, 50, 66, 90}

	var t = utilts.Now()

	for _, v := range loop {
		m := utilts.Millisecond(t, v)
		date := time.Unix(m, 0).Format(TIMEFORMAT)
		sl = append(sl, date)
	}

	return sl
}
